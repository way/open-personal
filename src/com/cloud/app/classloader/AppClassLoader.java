package com.cloud.app.classloader;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

import com.cloud.platform.util.Constants;

public class AppClassLoader {

	public static ClassLoader loadAppJar(String jarName) {
		ClassLoader classLoader = null;
		
		try{
			File jarFile = new File(Constants.getAppPath() + jarName);
			
			URL[] urls = new URL[1];
			urls[0] = jarFile.toURI().toURL();
			
			classLoader = new URLClassLoader(urls);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return classLoader;
	}
	
	public static Class loadClass(String className, ClassLoader classLoader) {
		Class clazz = null;
		
		try {
			clazz = Class.forName(className, true, classLoader);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return clazz;
	}
}
