package com.cloud.goal.model;

public class Habit {

	private String id;
	private String uid;
	private int sn;
	private String name;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUid() {
		return uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public int getSn() {
		return sn;
	}
	
	public void setSn(int sn) {
		this.sn = sn;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
