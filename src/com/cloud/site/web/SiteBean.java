package com.cloud.site.web;

import java.util.List;

import com.cloud.platform.BaseHandler;
import com.cloud.site.model.Catagory;
import com.cloud.site.model.Site;
import com.cloud.site.service.SiteService;
import com.cloud.site.vo.SiteVo;

public class SiteBean extends BaseHandler {

	private SiteService siteService;
	
	private Catagory catagory;
	private Site site;
	private List<SiteVo> sites;
	private List<Catagory> catagorys;
	
	public List<Catagory> getCatagorys() {
		if(catagorys == null) {
			catagorys = siteService.searchCatagorys(getLoginUserId());
		}
		return catagorys;
	}

	public List<SiteVo> getSites() {
		if(sites == null) {
			sites = siteService.searchSites(getLoginUserId());
		}
		return sites;
	}

	public void saveCatagory() {
		siteService.saveCatagory(catagory, getLoginUserId());
	}
	
	public void saveSite() {
		siteService.saveSite(site, getLoginUserId());
	}
	
	public Catagory getCatagory() {
		if(catagory == null) {
			catagory = siteService.getCatagory(getParameter("catagoryId"));
		}
		return catagory;
	}

	public Site getSite() {
		if(site == null) {
			site = siteService.getSite(getParameter("siteId"));
		}
		return site;
	}

	/**
	 * =======================  getters and setters  =======================
	 */
	public void setSite(Site site) {
		this.site = site;
	}
	
	public SiteService getSiteService() {
		return siteService;
	}

	public void setSiteService(SiteService siteService) {
		this.siteService = siteService;
	}
	
	public void setSites(List<SiteVo> sites) {
		this.sites = sites;
	}
	
	public void setCatagorys(List<Catagory> catagorys) {
		this.catagorys = catagorys;
	}
	
	public void setCatagory(Catagory catagory) {
		this.catagory = catagory;
	}
}
