package com.cloud.site.vo;

import java.util.List;

import com.cloud.site.model.Catagory;
import com.cloud.site.model.Site;

public class SiteVo {

	private Catagory catagory;
	private List<Site> sites;
	
	public Catagory getCatagory() {
		return catagory;
	}
	
	public void setCatagory(Catagory catagory) {
		this.catagory = catagory;
	}
	
	public List<Site> getSites() {
		return sites;
	}
	
	public void setSites(List<Site> sites) {
		this.sites = sites;
	}
}
