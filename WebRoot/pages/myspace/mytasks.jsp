<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.widget.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.mouse.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.table.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="createTask();return false;">创建任务</a>
		<a href="#" class="current">我接收的任务</a>
		<a href="finishTasks.jsf">已完成任务</a>
	</div>
	
	<div id="outerDiv" style="overflow: auto;">	
	<h:form id="operate">
		<table id="taskTab" class="list-table" cellpadding="0" cellspacing="0">
			<tr>
				<th width="50px">序号</th>
				<th width="50px">指示灯</th>
				<th width="60px">倒数天数</th>
				<th width="400px">名称</th>
				<th width="150px">所属节点</th>
				<th width="120px">优先级</th>
				<th width="100px">起始日期</th>
				<th width="100px">结束日期</th>
				<th width="100px">完成率</th>
			</tr>
			
			<a4j:repeat value="#{taskBean.mytasks}" var="item" rowKeyVar="row">
			<tr id="<h:outputText value="#{item.id}" />" ondblclick="openTask($(this));">
				<td class="sn"><h:outputText value="#{row + 1}" /></td>
				
				<h:panelGroup rendered="#{item.light == 0}">
					<td></td>
				</h:panelGroup>
				<h:panelGroup rendered="#{item.light == 1}">
					<td class="center"><img src="<c:url value="/images/light_green.png" />" /></td>
				</h:panelGroup>
				<h:panelGroup rendered="#{item.light == 2}">
					<td class="center"><img src="<c:url value="/images/light_yellow.png" />" /></td>
				</h:panelGroup>
				<h:panelGroup rendered="#{item.light == 3}">
					<td class="center"><img src="<c:url value="/images/light_red.png" />" /></td>
				</h:panelGroup>
				<h:panelGroup rendered="#{item.light == 4}">
					<td class="center"><img src="<c:url value="/images/light_gray.png" />" /></td>
				</h:panelGroup>
				
				<h:panelGroup rendered="#{item.days >= 0}">
					<td><h:outputText value="#{item.days}" /></td>
				</h:panelGroup>
				<h:panelGroup rendered="#{item.days < 0}">
					<td style="color: red;"><h:outputText value="#{item.days}" /></td>
				</h:panelGroup>
				
				<td class="auto-link">
					<a href="#" onclick="$(this).parent().dblclick();" <h:outputText value="class='require'" rendered="#{item.readed == 'N'}" />><h:outputText value="#{item.name}" /></a>
				</td>
				
				<td><h:outputText value="#{item.nodeName}" /></td>
				<td><h:outputText value="#{item.priority}" converter="GoalPrioConverter" /></td>
				<td><h:outputText value="#{item.startDate}" converter="DateConverter" /></td>
				<td><h:outputText value="#{item.endDate}" converter="DateConverter" /></td>
				<td><h:outputText value="#{item.rate}" />%</td>
			</tr>
			</a4j:repeat>
		</table>
	
		<a4j:commandLink id="refresh" reRender="operate" />
	</h:form>
	</div>
	
	<script>
		var tableMain;
		initTab();
		$("#outerDiv").height(getViewH());
		
		function initTab() {
			tableMain = $("#taskTab").table();			
		}
		
		function openTask($tr) {
			buildPage("taskFrm", basePath + "pages/myspace/taskReport.jsf?taskId=" + $tr.attr("id"));
		}
		
		function createTask() {
			buildPage("taskFrm", basePath + "pages/goal/taskCreate.jsf?");
		}
		
		function refresh() {
			$("#operate\\:refresh").click();
		}
	</script>
</body>
</html>
</f:view>