<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveReport();return false;">保存</a>
		<a href="#" onclick="clearPage('taskFrm');return false;">返回</a>
	</div>
	
	<div style="padding: 5px;"><h:outputText value="#{taskBean.task.name}" /></div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">投入工作量</td>
				<td class="edit-half">
					<h:inputText id="name" value="#{taskBean.taskReport.effort}" styleClass="edit-require" />
				</td>
				<td class="edit-label">完成率</td>
				<td class="edit-half">
					<h:inputText id="names" value="#{taskBean.task.rate}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">备注</td>
				<td class="edit-whole" colspan="3">
					<h:inputTextarea id="remark" value="#{taskBean.taskReport.intro}" style="height: 160px;" />
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="taskId" value="${param.taskId}" />
		
		<a4j:commandLink id="save" action="#{taskBean.saveTaskReport}" oncomplete="clearPage('taskFrm', true);" />
	</h:form>
	
	<div class="split-bar">基本信息</div>
	
	<jsp:include page="../goal/taskEntity.jsp?taskId=${param.taskId}" />
	
	<div class="split-bar">执行日报</div>
	
	<a4j:repeat value="#{taskBean.taskReports}" var="report">
		<div style="width: 810px;padding: 5px;border-bottom: 1px dotted #ccc;">
			填写日期：<h:outputText value="#{report.reportDate}" converter="DateConverter" />&nbsp;&nbsp;&nbsp;
			投入工作量：<h:outputText value="#{report.effort}" />&nbsp;&nbsp;&nbsp;
			完成率：<h:outputText value="#{report.rate}" />%
			<div style="padding: 5px 10px;"><h:outputText value="#{report.intro}" /></div>
		</div>
	</a4j:repeat>
	
	<script>
		function saveReport() {
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>