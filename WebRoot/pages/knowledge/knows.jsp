<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/jquery-ui.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/colorpicker.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-ui.min.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/json2.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-menu.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/colorpicker.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.knowtable.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="clearPage('kngFrm');return false;">返回</a>
		<a href="#" onclick="refresh();return false;">刷新</a>
		<img src="<c:url value="/images/004.png" />" class="garbage-img" title="回收列表" />
	</div>
	
	<div style="padding: 5px;">
		<div style="float: left;">
			<input id="saveBtn" type="button" class="button" value="保存" disabled="true" onclick="save();" />
			<input type="button" class="button" value="新增知识" onclick="addKnow();" />
			<input type="button" class="button" value="新增节点" onclick="tableMain.table('addKnow');" />
			<input type="button" class="button" value="升级" onclick="tableMain.table('levelUp');" />
			<input type="button" class="button" value="降级" onclick="tableMain.table('levelDown');" />
		</div>
		
		<div style="float: left;padding-left: 18px;padding-bottom: 3px;margin-top: -3px;;">
			<input id="colorBtn" type="text" value="颜色" style="width: 80px;color: #999;" />
		</div>
	</div>
	
	<div id="outerDiv" style="overflow: auto;clear: both;">
	<h:form id="operate">
		<table id="knowTab" class="list-table" cellpadding="0" cellspacing="0" style="border-top: 1px solid #999;clear: both;">
			<tr>
				<th width="50px">序号</th>
				<th width="40px"><img class="star-img" src="<c:url value="/images/003.png" />" title="关注" /></th>
				<th width="500px">名称</th>
				<th width="150px">更新时间</th>
			</tr>
			
			<a4j:repeat value="#{knowBean.knows}" var="item" rowKeyVar="row">
			<tr id="<h:outputText value="#{item.id}" />" level="<h:outputText value="#{item.level}" />" 
				isNode="<h:outputText value="#{item.isNode}" />"
				color="<h:outputText value="#{item.color}" />"
				style="background-color: <h:outputText value="#{item.color}" />;"
				ondblclick="openKnow($(this));">
				
				<td class="sn"><h:outputText value="#{row + 1}" /></td>
				<td class="sn"><img class="star-img" src="<c:url value="/images/002.png" />" title="关注" /></td>
				
				<h:panelGroup rendered="#{item.isNode == 'Y'}">
					<td><div style="font-weight: bold;padding-left: <h:outputText value="#{item.pad}" />px;"><h:outputText value="#{item.name}" /></div></td>
				</h:panelGroup>
				
				<h:panelGroup rendered="#{item.isNode == 'N'}">
					<td><div style="padding-left: <h:outputText value="#{item.pad}" />px;" class="auto-link"><a href="#" onclick="$(this).closest('tr').dblclick();return false;"><h:outputText value="#{item.name}" /></a></div></td>
				</h:panelGroup>
				
				<td><h:outputText value="#{item.modifyTime}" converter="TimeConverter" /></td>
			</tr>
			</a4j:repeat>
		</table>
	
		<input type="hidden" id="_knowledgeId" name="knowledgeId" value="${param.knowledgeId}" />
		<input type="hidden" id="_info" name="knowInfo" />
		
		<a4j:commandLink id="save" action="#{knowBean.save}" />
		<a4j:commandLink id="refresh" reRender="operate" oncomplete="initTab();" />
	</h:form>
	</div>
	
	<div id="menu"></div>
	
	<script>
		var tableMain;
		initTab();
		$("#outerDiv").height(getViewH() - 31);
		
		$("#colorBtn").colorpicker().on("change.color", function(event, color) {
			tableMain.table("color", color);
		});
		
		$("#menu").omMenu({ 
			contextMenu: true,
			
			dataSource: [{id: "1", label: "打开", action: "open", seperator: true}, 
			             {id: "2", label: "删除", action: "del"}],
			             
			onSelect: function(item, e) {
				var $tr = tableMain.table("getRow");
				
				if(item.action == "open") {
					openKnow($tr);
				}
				
				if(item.action == "del") {
					var id = $tr.attr("id");
					
					if(!id) { $tr.remove(); }
					else { $tr.attr("isDel", "Y").hide(); }
					
					tableMain.table("resetSn");
					$("#saveBtn").attr("disabled", false);
				}
			}
		});
		
		function initTab() {
			tableMain = $("#knowTab").table({ menu: "menu" });			
		}
		
		function addKnow() {
			// get parent id
			var parentId = "", $tr = $("#knowTab tr.tr-selected");
			
			if($tr.size() > 0) {
				if($tr.attr("isNode") == "Y") {
					parentId = $tr.attr("id");
				} else {
					var $pTr = $tr.prevAll("tr[isNode='Y']");
					
					if($pTr.size() > 0) {
						$pTr.each(function() {
							if(parseInt($(this).attr("level")) < parseInt($tr.attr("level"))) {
								parentId = $(this).attr("id");  return false;
							}
						});
					}
				}
			}
			
			// get previous row id for sn sort
			var snid = $tr.size() > 0 ? $tr.attr("id") : "";
			
			buildPage("knowFrm", basePath + "pages/knowledge/knowAdd.jsf?knowledgeId="
					+ $("#_knowledgeId").val() + "&parentId=" + parentId + "&snid=" + snid);
		}
		
		function openKnow($tr) {
			buildPage("knowEntityFrm", basePath + "pages/knowledge/know.jsf?knowId=" + $tr.attr("id"));
		}
		
		function save() {
			if($("#saveBtn").attr("disabled"))  return;
				
			var datas = [], data;
			
			$("#knowTab tr[isDel='Y']").each(function() {
				data = {i: $(this).attr("id")};
				datas.push(data);
			});
			
			$("#knowTab tr[isEdit='Y']").each(function() {
				var $tr = $(this), $pTr = $tr.prev(), data = {};
				
				data.i = $tr.attr("id") ? $tr.attr("id") : "";
				data.pi = $tr.attr("parentId") ? $tr.attr("parentId") : "";
				data.c = $tr.attr("color") ? $tr.attr("color") : "";
				data.n = $("td:eq(2) div", $tr).text().trim();
				data.snid = $pTr.attr("id") ? $pTr.attr("id") : "";
				
				datas.push(data);
			});
			
			$("#saveBtn").attr("disabled", true);
			$("#_info").val(JSON.stringify(datas));
			$("#operate\\:save").click();
		}
		
		function refresh() {
			$("#operate\\:refresh").click();
		}
	</script>
</body>
</html>
</f:view>