<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/editor/kindeditor-min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/editor/lang/zh_CN.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveKnow();return false;">保存</a>
		<a href="#" onclick="clearPage('knowFrm');return false;">返回</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">名称</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{knowBean.know.name}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">内容</td>
				<td class="edit-whole" colspan="3">
					<textarea id="content" style="height: 510px;"><h:outputText value="#{knowBean.know.content}" /></textarea>
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="knowId" value="${param.knowId}" />
		<h:inputHidden id="knowId" value="#{knowBean.know.id}" />
		
		<input type="hidden" name="knowledgeId" value="${param.knowledgeId}" />
		<input type="hidden" name="parentId" value="${param.parentId}" />
		<input type="hidden" name="snid" value="${param.snid}" />
		
		<h:inputHidden id="content" value="#{knowBean.know.content}" />
		
		<a4j:commandLink id="save" action="#{knowBean.saveKnow}" oncomplete="clearPage('knowFrm', true);" />
	</h:form>
	
	<script>
		var editor;
		KindEditor.ready(function(K) {
			editor = K.create("#content", {resizeType: 0, uploadJson: "../../editor/jsp/upload_json.jsp"});
		});
		
		function saveKnow() {
			$("#operate\\:content").val(editor.html());
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>