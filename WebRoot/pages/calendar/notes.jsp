<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/notes.css" />" />
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-ui.min.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/jquery.stickynote.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/dwr/engine.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/util.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/__noteBean.js" />"></script>
	
	<style>
		body {background: #fcfef4 url(../../images/stickynote-bg.png) repeat-x top left;background-position-y: -100px;font-family: arial;padding: 0;margin: 0;}
		.content {position: absolute;top: 30px;left: 0px;right: 0px;padding: 0px;margin: 0px;}
		ul.demos {list-style-type: none;position: absolute;top: 0;left: 2px;padding: 0px;margin: 0px;z-index: 9999;}
		ul.demos li {background-color: #3C3C3C;display: inline;color: #FEFEE7;font-weight: bold;float: left;padding: 2px 4px 2px 4px;margin: 2px;border-radius: 0 0 5px 5px;cursor: pointer;border: 1px solid #ECEDD5;border-top:none;}
		.descr {position: absolute;font-size: 18px;color: #ccc;margin: 0 auto;top: 135px;text-align:center;width:100%;}
	</style>
</head>

<body>
	<ul class="demos">
		<li id="testclick">Big Note</li>
		<li id="testsmall">Small Note</li>
		<li id="testcolor">Red Note</li>
	</ul>
	
	<div id="content" class="content"></div>
	
	<div class="descr">猛烈双击屏幕也可创建便签哦~</div>
	
	<div style="display: none;">
		<span id="delTip">确定要删除该便签么？</span>
		
		<h:inputHidden id="noteInfo" value="#{noteBean.noteInfo}" />
	</div>	
		
	<script type="text/javascript">
		/**
		 * 三种情况下保存数据
		 * 1. 当点击便签的勾号时
		 * 2. 当移动打勾的便签时
		 * 3. 删除便签时
		*/
		
		$(function() {
			$("#content").height(getViewH() + 1).stickynote({
				size 			 : 'large',
				containment		 : 'content',
				event			 : 'dblclick'
			});
			
			$("#testclick").stickynote({
				size 			 : 'large',
				containment		 : 'content'
			});
			
			$("#testsmall").stickynote({
				containment		 : 'content'
			});
			
			$("#testcolor").stickynote({
				containment		 : 'content',
				color			: '#FF0000',
				ontop			: true
			});
			
			// init created notes
			initNotes();
		});
		
		function initNotes() {
			var info = $("#noteInfo").val();
			if(!info)  return;
			
			var notes = eval(info), n;
			
			for(var i in notes) {
				n = notes[i];
				$("#testclick").stickynote.createNote({id: n.id, text: n.content, color: n.color, size: n.size, containment: "content", left: n.offsetX, top: n.offsetY});				
			}
		}
		
		function saveNote($div, text, offset, color, size) {
			__noteBean.dynSaveNote(text, offset.left, offset.top - 30, color, size, {
				async: false,
				callback: function(data) {
					if(data && $div.size() > 0)  $div.attr("id", data);
				}
			});
		}
		
		function updateNote(id, offset) {
			__noteBean.dynUpdateNote(id, offset.left, offset.top - 30, {async: false});
		}
		
		function removeNote(id) {
			__noteBean.dynRemoveNote(id, {async: false});
		}
	</script>
</body>
</html>
</f:view>