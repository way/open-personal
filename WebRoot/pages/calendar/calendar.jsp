<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/fullcalendar.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-ui.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/fullcalendar.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/dwr/engine.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/util.js" />"></script>
	<script type="text/javascript" src="<c:url value="/dwr/interface/__calendarBean.js" />"></script>

	<style>
		#external-events {
			float: left;
			width: 150px;
			padding: 0 10px;
			margin: 10px;
			border: 1px solid #ccc;
			background: #eee;
			text-align: left;
		}
		#external-events h4 {
			font-size: 16px;
			margin-top: 0;
			padding-top: 1em;
		}
		.external-event { /* try to mimick the look of a real event */
			margin: 10px 0;
			padding: 2px 4px;
			background: #3366CC;
			color: #fff;
			font-size: .85em;
			cursor: pointer;
		}
		#external-events p {
			margin: 1.5em 0;
			font-size: 11px;
			color: #666;
		}
		#external-events p input {
			margin: 0;
			vertical-align: middle;
		}
	</style>
</head>

<body>
	<div id='external-events'>
		<h4>我的任务</h4>
		<p>拖放任务至日程中</p>
		
		<a4j:repeat value="#{taskBean.mytasks}" var="item">
			<div id="<h:outputText value="#{item.id}" />" class='external-event'><h:outputText value="#{item.name}" /></div>
		</a4j:repeat>
		
		<p><input type='checkbox' id='drop-remove' /> <label for='drop-remove'>拖放完后移除任务</label></p>
	</div>

	<div id='calendar' style="padding: 10px;float: right;"></div>
	
	<h:inputHidden id="events" value="#{calendarBean.events}" />
	
	<script>
		$(function() {
			/* initialize the external events
			-----------------------------------------------------------------*/
			$('#external-events div.external-event').each(function() {
			
				// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
				// it doesn't need to have a start or end
				var eventObject = {
					title: $.trim($(this).text()) // use the element's text as the event title
				};
				
				// store the Event Object in the DOM element so we can get to it later
				$(this).data('eventObject', eventObject);
				
				// make the event draggable using jQuery UI
				$(this).draggable({
					zIndex: 999,
					revert: true,      // will cause the event to go back to its
					revertDuration: 0  //  original position after the drag
				});
			});
		
			/* initialize the calendar
			-----------------------------------------------------------------*/
			$("#calendar").width(getViewW() - $("#external-events").outerWidth() - 40).fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				
				// load events
				events: eval($("#events").val()),
				
				editable: true,
				height: getViewH() + 13,
				droppable: true, // this allows things to be dropped onto the calendar !!!
				drop: function(date, allDay) { // this function is called when something is dropped
					
					var newId = "";
				
					// save the new event
					__calendarBean.dynSaveEvent(getDateStr(date), $(this).attr("id"), {
						async: false,
						callback: function(data) { newId = data; }
					});
				
					// retrieve the dropped element's stored Event Object
					var originalEventObject = $(this).data('eventObject');
					
					// we need to copy it, so that multiple events don't have a reference to the same object
					var copiedEventObject = $.extend({}, originalEventObject);
					
					// assign it the date that was reported
					copiedEventObject.id = newId;
					copiedEventObject.start = date;
					copiedEventObject.allDay = allDay;
					
					// render the event on the calendar
					// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
					$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
					
					// is the "remove after drop" checkbox checked?
					if ($('#drop-remove').is(':checked')) {
						// if so, remove the element from the "Draggable Events" list
						$(this).remove();
					}
				},
				eventDrop: function(event) {
					__calendarBean.dynUpdateEvent(event.id, getDateStr(event.start), getDateStr(event.end), {
						async: false,
						callback: function(data) {}
					});
				},
				eventResize: function(event) {
					__calendarBean.dynUpdateEvent(event.id, getDateStr(event.start), getDateStr(event.end), {
						async: false,
						callback: function(data) {}
					});
				},
				
				// for chinese
				monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
				monthNamesShort: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
		        dayNames: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],  
		        dayNamesShort: ["周日", "周一", "周二", "周三", "周四", "周五", "周六"],  
		        today: ["今天"],  
		        buttonText: {today: "今天", month: "月", week: "周", day: "日"}
			});
		});
	</script>
</body>
</html>
</f:view>