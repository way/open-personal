<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="savePlan();return false;">保存</a>
		<a href="#" onclick="clearPage('planFrm');return false;">返回</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">计划名称</td>
				<td class="edit-whole" colspan="3">
					<h:inputText value="#{planBean.plan.name}" styleClass="edit-require" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">起始日期</td>
				<td class="edit-whole" colspan="3" style="padding-left: 3px;">
					<input id="startDate" name="startDate" class="edit-timer" value="<h:outputText value="#{planBean.plan.startDate}" converter="DateConverter" />" />
				</td>
			</tr>
			<tr>
				<td class="edit-label">结束日期</td>
				<td class="edit-whole" colspan="3" style="padding-left: 3px;">
					<input id="endDate" name="endDate" class="edit-timer" value="<h:outputText value="#{planBean.plan.endDate}" converter="DateConverter" />" />
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="planId" value="${param.planId}" />
		<h:inputHidden id="planId" value="#{planBean.plan.id}" />
		
		<input type="hidden" name="goalId" value="${param.goalId}" />
		
		<a4j:commandLink id="save" action="#{planBean.savePlan}" oncomplete="clearPage('planFrm', true);" />
	</h:form>
	
	<script>
		$("#startDate, #endDate").omCalendar();
		
		function savePlan() {
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>