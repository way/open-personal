<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	
	<style>
		table {margin-left: 20px;}
		tr {height: 40px;}
		td {border-bottom: 1px dotted #ccc;}
	</style>
</head>

<body>
	<div class="menu-title">
		<a href="habits.jsf">习惯提交</a>
		<a href="#">习惯统计</a>
	</div>
	
	<script>
		init();
		
		function init() {

		}
	</script>
</body>
</html>
</f:view>