<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.widget.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.mouse.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.table.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-menu.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-mouse.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-draggable.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-position.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-messagebox.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-button.js" />"></script>
</head>

<body>
	<div style="padding: 5px;">
		<input type="button" class="button" value="新增计划" onclick="addPlan();" />
	</div>
	
	<h:form id="operate">
		<table id="planTab" class="list-table" cellpadding="0" cellspacing="0" style="border-top: 1px solid #999;">
			<tr>
				<th width="50px">序号</th>
				<th width="400px">名称</th>
				<th width="100px">起始日期</th>
				<th width="100px">结束日期</th>
				<th width="150px">创建时间</th>
				<th width="150px">更新时间</th>
			</tr>
			
			<a4j:repeat value="#{planBean.planList}" var="item" rowKeyVar="row">
			<tr id="<h:outputText value="#{item.id}" />" ondblclick="openPlan($(this));">
				<td class="sn"><h:outputText value="#{row + 1}" /></td>
				<td><span class="auto-link"><a href="#" onclick="openPlan($(this).closest('tr'));return false;"><h:outputText value="#{item.name}" /></a></span></td>
				<td><h:outputText value="#{item.startDate}" converter="DateConverter" /></td>
				<td><h:outputText value="#{item.endDate}" converter="DateConverter" /></td>
				<td><h:outputText value="#{item.createTime}" converter="TimeConverter" /></td>
				<td><h:outputText value="#{item.modifyTime}" converter="TimeConverter" /></td>
			</tr>
			</a4j:repeat>
		</table>
		
		<input type="hidden" id="planId" name="planId" />
		<input type="hidden" name="goalId" value="${param.goalId}" />
		<a4j:commandLink id="refresh" reRender="operate" oncomplete="initTab();" />
		<a4j:commandLink id="remove" action="#{planBean.removePlan}" oncomplete="refresh();" />
	</h:form>
	
	<div id="menu"></div>
	
	<script>
		var tableMain;
		initTab();
		
		$("#menu").omMenu({ 
			contextMenu: true,
			dataSource: [{id: "1", label: "编辑", action: "edit"}, {id: "2", label: "删除", action: "del"}],
			onSelect: function(item, e) {
				if(item.action == "del") {
					cloudConfirm(function() {
						$("#planId").val(tableMain.table("getRow").attr("id"));
						$("#operate\\:remove").click();
					});
				}
				
				if(item.action == "edit") {
					buildPage("planFrm", basePath + "pages/goal/planAdd.jsf?planId=" + tableMain.table("getRow").attr("id"), true);
				}
			}
		});
		
		function initTab() {
			tableMain = $("#planTab").table({ menu: "menu" });			
		}
	
		function addPlan() {
			buildPage("planFrm", basePath + "pages/goal/planAdd.jsf?goalId=${param.goalId}", true);
		}
		
		function openPlan($tr) {
			buildPage("tasksFrm", basePath + "pages/goal/tasks.jsf?planId=" + $tr.attr("id"), true);
		}
		
		function refresh() {
			$("#operate\\:refresh").click();
		}
	</script>
</body>
</html>
</f:view>