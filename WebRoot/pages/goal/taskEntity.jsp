<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<table class="edit-table">
	<tr>
		<td class="edit-label">任务名称</td>
		<td class="edit-half"><h:outputText value="#{taskBean.task.name}" /></td>
		
		<td class="edit-label">优先级</td>
		<td class="edit-half"><h:outputText value="#{taskBean.task.priority}" converter="GoalPrioConverter" /></td>
	</tr>
	<tr>
		<td class="edit-label">所属目标</td>
		<td class="edit-half"><h:outputText value="#{taskBean.task.goalName}" /></td>
		
		<td class="edit-label">所属计划</td>
		<td class="edit-half"><h:outputText value="#{taskBean.task.planName}" /></td>
	</tr>
	<tr>
		<td class="edit-label">起始日期</td>
		<td class="edit-half"><h:outputText value="#{taskBean.task.startDate}" converter="DateConverter" /></td>
		
		<td class="edit-label">结束日期</td>
		<td class="edit-half"><h:outputText value="#{taskBean.task.endDate}" converter="DateConverter" /></td>
	</tr>
	<tr>
		<td class="edit-label">任务描述 </td>
		<td class="edit-whole" colspan="3"><h:outputText value="#{taskBean.task.remark}" /></td>
	</tr>
	<tr>
		<td class="edit-label">验收标准</td>
		<td class="edit-whole" colspan="3"><h:outputText value="#{taskBean.task.acceptance}" /></td>
	</tr>
</table>
