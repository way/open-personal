<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<f:view>
<html>
<head>
	<title>PPM个人管理系统</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-panel.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-accordion.js" />"></script>
	
	<style>
		#header {height: 40px;background: rgb(0, 51, 102);color: white;padding: 7px 15px 0 10px;}
		#header .logo {font-size: 22px;font-weight: bold;margin-right: 30px;}
		#header .operate {float: right;display: inline-block;padding-top: 7px;}
		#header .operate a {color: white;text-decoration: none;}
		#header .operate a:hover {text-decoration: underline;}
		#mainContent {height: 600px;}
		#sidebar {float: left;width: 200px;border-right: 1px solid #999;}
		#sidebar .myspace_icon {background: url('<c:url value="/images/side/myspace.png" />') no-repeat;margin: -2px 5px 0 -5px;width: 24px;height: 24px;}
		#sidebar .calendar_icon {background: url('<c:url value="/images/side/calendar.png" />') no-repeat;margin: -2px 5px 0 -5px;width: 24px;height: 24px;}
		#sidebar .goal_icon {background: url('<c:url value="/images/side/goal.png" />') no-repeat;margin: -2px 5px 0 -5px;width: 24px;height: 24px;}
		#sidebar .knowledge_icon {background: url('<c:url value="/images/side/knowledge.png" />') no-repeat;margin: -2px 5px 0 -5px;width: 24px;height: 24px;}
		#sidebar .note_icon {background: url('<c:url value="/images/side/note.png" />') no-repeat;margin: -2px 5px 0 -5px;width: 24px;height: 24px;}
		#sidebar .manage_icon {background: url('<c:url value="/images/side/manage.png" />') no-repeat;margin: -2px 5px 0 -5px;width: 24px;height: 24px;}
		#content {margin-left: 208px;border-left: 1px solid #999;}
		#footer {clear: both;border-top: 1px solid #cccccc;text-align: center;padding: 5px;}
		#footer span {float: right;margin-right: 20px;}
	</style>
</head>

<body>
	<div id="header">
		<span class="logo">PPM Personal Manage</span>
		<span><h:outputText value="#{userBean.sign}" /></span>
		<span class="operate"><a href="../index.jsf">退出</a></span>
	</div>

	<div id="mainContent">
		<div id="sidebar">
			<ul>
				<li><a href="#accordion-1" iconCls="myspace_icon">个人空间</a></li>
				<li><a href="#accordion-2" iconCls="calendar_icon">日程管理</a></li>
				<li><a href="#accordion-3" iconCls="goal_icon">目标管理</a></li>
				<li><a href="#accordion-4" iconCls="knowledge_icon">知识管理</a></li>
				<li><a href="#accordion-5" iconCls="note_icon">日记相册</a></li>
				<li><a href="#accordion-6" iconCls="manage_icon">系统管理</a></li>
			</ul>
			<div id="accordion-1">
				<a href="#" url="<c:url value="/pages/myspace/mytasks.jsf" />" onclick="openMenu($(this));return false;">我的任务</a><br /><br />
				<a href="#" url="<c:url value="/pages/myspace/sites.jsf" />" onclick="openMenu($(this));return false;">网址导航</a><br /><br />
				<a href="#" url="<c:url value="/pages/myspace/apps.jsf" />" onclick="openMenu($(this));return false;">我的应用</a><br /><br />
			</div>
			<div id="accordion-2">
				<a href="#" url="<c:url value="/pages/calendar/calendar.jsf" />" onclick="openMenu($(this));return false;">我的日程</a><br /><br />
				<a href="#" url="<c:url value="/pages/calendar/notes.jsf" />" onclick="openMenu($(this));return false;">备忘墙</a><br /><br />
			</div>
			<div id="accordion-3">
				<a href="#" url="<c:url value="/pages/goal/goalList.jsf" />" onclick="openMenu($(this));return false;">目标管理</a><br /><br />
				<a href="#" url="<c:url value="/pages/goal/habits.jsf" />" onclick="openMenu($(this));return false;">习惯养成</a><br /><br />
			</div>
			<div id="accordion-4">
				<a href="#" url="<c:url value="/pages/knowledge/knowledges.jsf" />" onclick="openMenu($(this));return false;">我的知识</a><br /><br />
			</div>
			<div id="accordion-5">
				<a href="#" url="<c:url value="/pages/note/diary.jsf" />" onclick="openMenu($(this));return false;">我的日记</a><br /><br />
				<a href="#" url="<c:url value="/pages/note/pics.jsf" />" onclick="openMenu($(this));return false;">我的相册</a><br /><br />
			</div>
			<div id="accordion-6">
				<a href="#" url="<c:url value="/pages/sys/sign.jsf" />" onclick="openMenu($(this));return false;">个性签名</a><br /><br />
				<a href="#" url="<c:url value="/pages/sys/account.jsf" />" onclick="openMenu($(this));return false;">修改密码</a><br /><br />
			</div>
		</div>

		<div id="content">
			<iframe id="mainFrame" name="mainFrame" width="100%" height="100%" frameborder="0"></iframe>
		</div>
	</div>

	<div id="footer">
		PPM Personal Manage
		<span>当前登录：<%=session.getAttribute("LOGIN_USERNAME") %></span>
	</div>
	
	<div class="hide">
		<span id="delTle">确认删除</span>
		<span id="delTip">确定要删除该条记录吗？</span>
		<span id="selectText">请选择</span>
	</div>
	
	<script>
		$(function() {
			$("#sidebar").omAccordion({
				width: 200,
				height: 601,
				switchEffect: true,
				onActivate: function(n, event) {
					var $a = $("#accordion-" + (n + 1) + " a:eq(0)");
					openMenu($a);
				}
			});
			
			openMenu($("#accordion-1 a:eq(0)"));
		});
		
		function openMenu($a) {
			$("#mainFrame").attr("src", $a.attr("url"));
		}
	</script>
</body>
</html>
</f:view>