﻿/* SQL for Derby database. */;

CREATE TABLE t_app_info (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  type int default NULL,
  info varchar(2048) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_cal_evt (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  taskId varchar(36) default NULL,
  startDate varchar(255) default NULL,
  endDate varchar(255) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_cal_note (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  content varchar(2048) default NULL,
  offsetX int default NULL,
  offsetY int default NULL,
  color varchar(255) default NULL,
  size varchar(255) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_goal_goal (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  name varchar(255) default NULL,
  startDate date default NULL,
  endDate date default NULL,
  type int default NULL,
  priority int default NULL,
  remark varchar(2048) default NULL,
  createTime varchar(36) default NULL,
  modifyTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_goal_plan (
  Id varchar(36) NOT NULL default '',
  goalId varchar(36) default NULL,
  name varchar(255) default NULL,
  startDate date default NULL,
  endDate date default NULL,
  createTime varchar(36) default NULL,
  modifyTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_goal_task (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  planId varchar(36) default NULL,
  color varchar(255) default NULL,
  sn int default NULL,
  rate int default NULL,
  status int default NULL,
  isNode varchar(1) default NULL,
  parentId varchar(36) default NULL,
  readed varchar(1) default NULL,
  name varchar(255) default NULL,
  startDate date default NULL,
  endDate date default NULL,
  realEndDate date default NULL,
  priority int default NULL,
  remark varchar(2048) default NULL,
  acceptance varchar(2048) default NULL,
  createTime varchar(36) default NULL,
  modifyTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_kng_kng (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  name varchar(255) default NULL,
  sn int default NULL,
  color varchar(255) default NULL,
  isNode varchar(1) default NULL,
  parentId varchar(36) default NULL,
  createTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_kng_know (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  knowledgeId varchar(36) default NULL,
  name varchar(255) default NULL,
  content long varchar,
  sn int default NULL,
  color varchar(255) default NULL,
  isNode varchar(1) default NULL,
  isValid varchar(1) default NULL,
  parentId varchar(36) default NULL,
  createTime varchar(36) default NULL,
  modifyTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_site_cat (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  sn int default NULL,
  name varchar(255) default NULL,
  createTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_site_site (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  sn int default NULL,
  catagoryId varchar(36) default NULL,
  name varchar(255) default NULL,
  url varchar(255) default NULL,
  createTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_sys_user (
  Id varchar(36) NOT NULL default '',
  username varchar(255) default NULL,
  password varchar(255) default NULL,
  email varchar(255) default NULL,
  sign varchar(255) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_task_report (
  Id varchar(36) NOT NULL default '',
  taskId varchar(36) default NULL,
  effort float default NULL,
  rate int default NULL,
  intro varchar(2048) default NULL,
  reportDate date default NULL,
  createTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_goal_habit (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  sn int default NULL,
  name varchar(255) default NULL,
  PRIMARY KEY  (Id)
);

CREATE TABLE t_goal_hbtdly (
  Id varchar(36) NOT NULL default '',
  uid varchar(36) default NULL,
  habitId varchar(36) default NULL,
  createTime varchar(36) default NULL,
  PRIMARY KEY  (Id)
);

insert into t_sys_user(Id,username,password,email) values('000000000000000000000000000000000000', 'admin', '1', '');
